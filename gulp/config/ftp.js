import { parallel } from "gulp";

export let configFTP = {
	host: "", // Адрес FTP сервера
	user: "", // Імя користувача
	password: "", // Пароль
	parallel: 5 // Кількість одночасних потоків
}