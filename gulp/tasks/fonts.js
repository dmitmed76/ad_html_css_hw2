// Плагін Nods який є в системі
import fs from 'fs';

// Плагіни конвертують в ttf woff
import fonter from 'gulp-fonter';

// Плагіни конвертують в ttf2 woff2
import ttf2woff2 from 'gulp-ttf2woff2';

export const otfToTtf = () => {
	// Шукаєм файли шрифтів .otf
	return app.gulp.src(`${app.path.srcFolder}/fonts/*.otf`, {})
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "FONTS",
				message: "Error: <%= error.message %>"
			}))
		)
		// Конвертуєм в .ttf
		.pipe(fonter({
			formats: ['ttf']
		}))
		// Завантажуєм в папку виходу
		.pipe(app.gulp.dest(`${app.path.srcFolder}/fonts/`))
}

export const ttfToWoff = () => {
	// Шукаєм файли шрифтів .ttf
	return app.gulp.src(`${app.path.srcFolder}/fonts/*.ttf`, {})
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "FONTS",
				message: "Error: <%= error.message %>"
			}))
		)
		// Конвертуєм в .woff
		.pipe(fonter({
			formats: ['woff']
		}))
		// Завантажуєм в папку виходу
		.pipe(app.gulp.dest(`${app.path.srcFolder}/fonts/`))

		// Шукаєм файли шрифтів .ttf
		.pipe(app.gulp.src(`${app.path.srcFolder}/fonts/*.ttf`))

		// Конвертуєм в .woff
		.pipe(ttf2woff2())

		// Завантажуєм в папку виходу
		.pipe(app.gulp.dest(`${app.path.build.fonts}`))
}

export const fontsStyle = () => {
	//Файл стилей подключення шрифтів
	let fontsFile = `${app.path.srcFolder}/scss/fonts.scss`;
	//Перевіряєм, чи існують файли шрифтів
	fs.readdir(app.path.build.fonts, function (err, fontsFiles) {
		if (fontsFiles) {
			//Перевіряєм чи існує файл стилів
			if (!fs.existsSync(fontsFile)) {
				//Если файла нет, создаём его
				fs.writeFile(fontsFile, '', cb);
				let newFileOnly;
				for (var i = 0; i < fontsFiles.length; i++) {
					//Записуєм підключення шрифтів в файл стилів
					let fontFileName = fontsFiles[i].split('.')[0];
					if (newFileOnly !== fontFileName) {
						let fontName = fontFileName.split('-')[0] ? fontFileName.split('-')[0] : fontFileName;
						let fontWeight = fontFileName.split('-')[1] ? fontFileName.split('-')[1] : fontFileName;
						if (fontWeight.toLowerCase() === 'thin') {
							fontWeight = 100;
						} else if (fontWeight.toLowerCase() === 'extralight') {
							fontWeight = 200;
						} else if (fontWeight.toLowerCase() === 'light') {
							fontWeight = 300;
						} else if (fontWeight.toLowerCase() === 'medium') {
							fontWeight = 500;
						} else if (fontWeight.toLowerCase() === 'semibold') {
							fontWeight = 600;
						} else if (fontWeight.toLowerCase() === 'bold') {
							fontWeight = 700;
						} else if (fontWeight.toLowerCase() === 'extrabold' || fontWeight.toLowerCase() === 'heavy') {
							fontWeight = 800;
						} else if (fontWeight.toLowerCase() === 'black') {
							fontWeight = 900;
						} else {
							fontWeight = 400;
						}
						fs.appendFile(fontsFile, `@font-face{\n\tfont-family: ${fontName};\n\tfont-display: swap;\n\tsrc: url("../fonts/${fontFileName}.woff2") format("woff2"), url("../fonts/${fontFileName}.woff") format("woff");\n\tfont-weight: ${fontWeight};\n\tfont-style: normal;\n}\r\n`, cb);
						newFileOnly = fontFileName;
					}
				}
			} else {
				//Якщо файл існує виводим повідомлення 
				console.log("Файл scss/fonts.scss існує. Для оновлення файла треба его видалити!");
			}
		}
	});
	return app.gulp.src(`${app.path.srcFolder}`);
	function cb() { }

}